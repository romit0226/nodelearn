const express=require('express')

const app=express();
const port = 3000;


//parse
app.use(express.json());
app.use(express.urlencoded({extended:false}));




let movies=[

    {
        id:'1',
        title:'Rango',
        director:'Nohan',
        release_date:"2012-07-24"
    },
    {
        id:'2',
        title:'FnF',
        director:'Right',
        release_date:"2016-07-24"
    },

];

// get list of movies

app.get('/movie',(req,res)=>{
    res.json(movies)
})

// add movie to list
app.post('/movie',(req,res)=>{
    const movie=req.body
    console.log(movie)
    movies.push(movie)
    res.send('Movie  id added to list')
})

// search for a movie
app.get('/movie/:id',(req,res)=>{
    const id=req.params.id
    for(let movie of movies){
        if(movie.id===id){
            res.json(movie)
            return
        }
    }
    res.status(404).send("Movie Not Found")
})
//remove from list
app.delete('/movie/:id',(req,res)=>{
    const id=req.params.id
    movies=movies.filter(movie=>{
        if(movie.id!==id){
            return true
        }
        return false
    })
    res.send('Movie is deleted')
})
// set the server to listen 
app.listen(port,()=>{
    console.log('server at at port ${port}')
});