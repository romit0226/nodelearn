const express=require('express');
const app=express();
const jwt=require('jsonwebtoken');
app.use(express.json());
app.use(express.urlencoded({extended:false}));

app.get('/api',(req,res)=>{
    res.json({
        message:"Hey There! Welcome to JWT"
    });
});

const users = [
    {
    id:'1',
    username:'Romit',
    email:'romit@gmail.com',
    },
]

app.get('/api/users',(req,res)=>{    
    res.json(users)
})

app.post('/api/users',(req,res)=>{
    const user=req.body
    console.log(user)
    users.push(user)
    res.send('user  id added to list')
})

// search
app.get('/api/users/:id',(req,res)=>{
    const id=req.params.id
    for(let user of users){
        if(user.id===id){
            res.json(user)
            return
        }
    }
    res.status(404).send("user Not Found")
})

// delete
// app.delete('/api/users/:id',(req,res)=>{
//     const id=req.params.id
//     users=users.filter(user=>{
//         if(user.id!==id){
//             return true
//         }
//         return false
//     })
//     res.send('user is deleted')
// })





app.post('/api/posts',verifyToken,(req,res)=>{
    jwt.verify(req.token,'secretkey',(err,authData)=>{
        if(err){
            res.sendStatus(403)
        }else{
            res.json({
                message:"Post Created..",
                authData,
            });
        }
    });
    
});

app.post('/api/login',(req,res)=>{
  
    jwt.sign({users:users},'secretkey',(err,token)=>{
        res.json({
            token,
        });
    });
});


function verifyToken(req,res,next){
    const bearerHeader=req.headers['authorization']
    if(typeof bearerHeader !=='undefined'){
          const bearertoken=bearerHeader.split(' ')[1]
          req.token=bearertoken
          next()    
        }else{
            res.sendStatus(403)
        }
}


app.listen(3000,(req,res)=>{
    console.log("Server Started at 3000")
})